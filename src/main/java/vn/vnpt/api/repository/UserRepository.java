package vn.vnpt.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.vnpt.api.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
}
