package vn.vnpt.api.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import vn.vnpt.api.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String>{
	List<Customer> findByLastName(String lastNamee);
	Customer findByUuidAccount(String uuidAccount);
	
}
