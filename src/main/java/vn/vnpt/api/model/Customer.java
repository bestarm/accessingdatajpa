package vn.vnpt.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customer")
public class Customer {
	
    @Id
    @NotNull
    @Column(name = "UUID_ACCOUNT", nullable = false)
    private String uuidAccount;
    
    @Column(name = "BIRTHDAY")
    private String birthDay;
    
    @Column(name = "CREDENTIALS_EXPIRED")
    private String credentialsExpired;
    
    @Column(name = "ENABLE")
    private int enable;
    
    @Column(name = "EXPIRED")
    private int expired;
    
    @Column(name = "FIRSTNAME")
    @NotNull(message = "Name field must not null")
    private String firstName;
    
    @Column(name = "FULLNAME")
    @NotNull(message = "Full name must not null")
    private String fullName;
    
    @Column(name = "GENDER")
    private String gender;
    
    @Column(name = "LAST_NAME")
    @NotNull(message = "Last name must not null")
    private String lastName;
    
    @Column(name = "LOCATION")
    private String location;
    
    @Column(name = "LOCKED")
    private int locked;
    
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;
    
    @Column(name = "PROVIDER")
    private String provider;
    
    @Column(name = "USERNAME")
    private String username;
    
    @Column(name = "PASSWORD")
    private String password;
    
    @Column(name = "USING2FA")
    private String using2fa;
    
    public String getUuidAccount() {
		return uuidAccount;
	}

	public void setUuidAccount(String uuidAccount) {
		this.uuidAccount = uuidAccount;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(String credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public int getEnable() {
		return enable;
	}

	public void setEnable(int enable) {
		this.enable = enable;
	}

	public int getExpired() {
		return expired;
	}

	public void setExpired(int expired) {
		this.expired = expired;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getLocked() {
		return locked;
	}

	public void setLocked(int locked) {
		this.locked = locked;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsing2fa() {
		return using2fa;
	}

	public void setUsing2fa(String using2fa) {
		this.using2fa = using2fa;
	}

	@Override
    public String toString() {
        return String.format(
                "Customer[UUID_ACCOUNT=%s, FIRSTNAME='%s', LASTNAME='%s']",
                uuidAccount, firstName, lastName);
    }
}
