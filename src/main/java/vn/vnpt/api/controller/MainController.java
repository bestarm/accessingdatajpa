package vn.vnpt.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import vn.vnpt.api.model.Customer;
import vn.vnpt.api.model.User;
import vn.vnpt.api.repository.CustomerRepository;
import vn.vnpt.api.repository.UserRepository;
import vn.vnpt.common.exception.InternalServerErrorException;
import vn.vnpt.common.exception.StandardError;
import vn.vnpt.common.response.StandardResponse;
import vn.vnpt.dto.in.UserDtoIn;
import vn.vnpt.dto.out.GetDtoOut;

@ComponentScan("vn.vnpt")
@RestController
public class MainController {
	
	private static final String SUCCESS_RESULT="success";
	private static final String FAILURE_RESULT="failure";

    @Autowired
    private CustomerRepository CustomerRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public String postCustomer(@RequestBody Customer customer) {
    	CustomerRepository.save(customer);
    	return SUCCESS_RESULT;
    }
    
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User postUser(@RequestBody @Valid UserDtoIn userInput) {
    	User user = new User();
    	user.setFirstName(userInput.getFirstName());
    	user.setLastName(userInput.getLastName());
    	user.setMiddleName(userInput.getMiddleName());
    	user.setSiblings(userInput.getSiblings());
    	return userRepository.saveAndFlush(user);
    }
    
    // Get Customer with pagination (tested OK !)
    @RequestMapping(value = "/customer",method = RequestMethod.GET)
    public StandardResponse<List<GetDtoOut>> getAllCustomer(@RequestParam(value = "pageNum") int pageNum,
    		@RequestParam(value = "pageSize") int pageSize) {
    	Pageable pageable = new PageRequest(pageNum, pageSize);
    	Page<Customer> page = CustomerRepository.findAll(pageable);
    	List<Customer> listAllCustomer = page.getContent();
    	
    	List<GetDtoOut> listAllDtoOut = new ArrayList<GetDtoOut>();
    	for(Customer customer : listAllCustomer) {
    		GetDtoOut element = getModelMapper().map(customer, GetDtoOut.class);
    		listAllDtoOut.add(element);
    	}
    	
    	StandardResponse<List<GetDtoOut>> response = new StandardResponse<List<GetDtoOut>>();
    	response.setValue(listAllDtoOut);
    	
    	return response;
    }
    
    @RequestMapping(value = "/customer/{index}", method = RequestMethod.DELETE)
    public String deleteCustomer(@PathVariable(value = "index") String uuidAccount ) {
    	Customer found = CustomerRepository.findByUuidAccount(uuidAccount);
    	if(found == null) {
    		System.out.println("Index was want to delete was not found");
    		return FAILURE_RESULT;
    	}else {
    		CustomerRepository.delete(uuidAccount);
    		return SUCCESS_RESULT;
    	}
    }
    
    @RequestMapping(value = "/customer/{index}", method = RequestMethod.PUT)
    public String putCustomer(@Valid @RequestBody Customer customer, @PathVariable(value = "index") String uuidAccount) throws InternalServerErrorException {
    	Customer foundCustomer = CustomerRepository.findByUuidAccount(uuidAccount);
    	if(foundCustomer == null) {
    		List<StandardError> listError = new ArrayList<StandardError>();
    		listError.add(new StandardError());
    		System.out.println("throw InternalServerErrorException");
    		throw new InternalServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
    				"No Customer Found", HttpStatus.INTERNAL_SERVER_ERROR.value()+"",listError) ;
    	}
    	foundCustomer.setFirstName(customer.getFirstName());
    	foundCustomer.setLastName(customer.getLastName());
    	CustomerRepository.flush();
    	return SUCCESS_RESULT;
    	
    }
    
    private ModelMapper getModelMapper() {
    	return new ModelMapper();
    }
    
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		return ex.getBindingResult()
			.getAllErrors().stream()
			.map(ObjectError::getDefaultMessage)
			.collect(Collectors.toList());
	}
}
