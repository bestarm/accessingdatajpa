package vn.vnpt;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@SpringBootApplication(scanBasePackages = "vn.vnpt")
public class AccessingDataJpaApplication {
	
	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class, args);
	}
	
    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource);
        return validatorFactoryBean;
    }
	
//	@Bean
//	public CommandLineRunner demo(CustomerRepository repository) {
//		return (args) -> {
//			// save a couple of customers
//			repository.save(new Customer("Jack", "Bauer"));
//			repository.save(new Customer("Chloe", "O'Brian"));
//			repository.save(new Customer("Kim", "Bauer"));
//			repository.save(new Customer("David", "Palmer"));
//			repository.save(new Customer("Michelle", "Dessler"));
//			repository.save(new Customer("Thanh", "Cong"));
//			repository.save(new Customer("Cong", "Ly"));
//			repository.save(new Customer("Dinh", "Viet"));
//			repository.save(new Customer("Huy", "Binh"));
//			repository.save(new Customer("Duc", "Tu"));
//			// fetch all customers
//			log.info("Customers found with findAll():");
//			log.info("-------------------------------");
//			for (Customer customer : repository.findAll()) {
//				log.info(customer.toString());
//			}
//			log.info("");
//
//			// fetch an individual customer by ID
//			Optional.ofNullable(repository.findById(1L))
//				.ifPresent(customer -> {
//						log.info("Customer found with findById(1L):");
//						log.info("--------------------------------");
//						log.info(customer.toString());
//						log.info("");
//				});
//
//			// fetch customers by last name
//			log.info("Customer found with findByIdGreaterThan(2L):");
//			log.info("--------------------------------------------");
//			repository.findByIdGreaterThan(2L).forEach(bauer -> {
//				log.info(bauer.toString());
//			});
//			// for (Customer bauer : repository.findByLastName("Bauer")) {
//			// 	log.info(bauer.toString());
//			// }
//			log.info("");
//			log.info("Customers found with findAll() have paging:");
//			log.info("-------------------------------------------");
//			Pageable firstPageWithTwoElements = new PageRequest(0, 2);			 
////			Pageable secondPageWithFiveElements = new PageRequest(1, 5);
//			
//			for(Customer customer : repository.findAll(firstPageWithTwoElements)) {
//				log.info(customer.toString());
//			}
//			log.info("");
//		};
//	}

}
