package vn.vnpt.common.response;

import java.util.List;

import vn.vnpt.common.exception.StandardError;

public class ErrorResponse{
	private String status;
	private String message;
	private String statusCode;
	
	private List<StandardError> listError;
	
	

	public ErrorResponse(String status, String message, String statusCode,
			List<StandardError> listError) {
		super();
		this.status = status;
		this.message = message;
		this.statusCode = statusCode;
		this.listError = listError;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<StandardError> getListError() {
		return listError;
	}

	public void setListError(List<StandardError> listError) {
		this.listError = listError;
	}
}
