package vn.vnpt.common.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageObject {

	private String objectName;
	private String message;
}
