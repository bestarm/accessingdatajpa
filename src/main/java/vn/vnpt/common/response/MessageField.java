package vn.vnpt.common.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageField {
	private String fieldName;
	private String message;
}
