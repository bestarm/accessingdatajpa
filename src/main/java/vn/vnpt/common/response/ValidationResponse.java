package vn.vnpt.common.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import vn.vnpt.common.errorcode.MessageType;

@Data
@AllArgsConstructor
public class ValidationResponse {
	private List<MessageField> messageFields;
	private List<MessageObject> messageObjects;
	private String statusCode;
	private String message;
	private String status;
	private MessageType error;
	
}
