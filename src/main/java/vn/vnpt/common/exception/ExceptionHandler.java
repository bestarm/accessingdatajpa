package vn.vnpt.common.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.common.errorcode.MessageType;
import vn.vnpt.common.response.ErrorResponse;
import vn.vnpt.common.response.MessageField;
import vn.vnpt.common.response.MessageObject;
import vn.vnpt.common.response.ValidationResponse;

@ControllerAdvice
public class ExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
		
	@org.springframework.web.bind.annotation.ExceptionHandler(InternalServerErrorException.class)
	@ResponseBody
	public ErrorResponse handleException(InternalServerErrorException ex) {
		ErrorResponse exception = new ErrorResponse(ex.getStatus(),
				ex.getMessage(),ex.getStatusCode(),ex.getListError());
		return exception;
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public ValidationResponse handleValidationException(MethodArgumentNotValidException ex){
		
		List<MessageField> messageFields = new ArrayList<MessageField>();
		List<MessageObject> messageObjects = new ArrayList<MessageObject>();
		Locale currentLocale = LocaleContextHolder.getLocale();
		for(FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
			String fieldName = "";
			String message = "";
			
			fieldName = fieldError.getField();
			try {				
				message = messageSource.getMessage(fieldError.getDefaultMessage(), null, currentLocale);
				System.out.println("messageField = messageSource.getMessage");
			}catch(Exception e) {
				System.out.println("messageField = fieldError.getDefaultMessage");
				e.printStackTrace();
				message = fieldError.getDefaultMessage();
			}
			messageFields.add(new MessageField(fieldName,message));
		}
		
		for(ObjectError objectError : ex.getBindingResult().getGlobalErrors()) {
			String objectName = objectError.getObjectName();
			String message = "";
			try {
				message = messageSource.getMessage(objectError.getDefaultMessage(), null, currentLocale);
				System.out.println("messageObject = messageSource.getMessage");
			}catch(Exception e) {
				System.out.println("messageObject = objectError.getMessage");
				e.printStackTrace();
				message = objectError.getDefaultMessage();
			}
			messageObjects.add(new MessageObject(objectName,message));
		}
		
		ValidationResponse validationDtoOut = new ValidationResponse(messageFields,
				messageObjects,HttpStatus.BAD_REQUEST.value()+"","IDG-00000400",HttpStatus.BAD_REQUEST.name(), MessageType.ERROR);
		return validationDtoOut;
	}
}
