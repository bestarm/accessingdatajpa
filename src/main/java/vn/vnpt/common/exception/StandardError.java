package vn.vnpt.common.exception;


public class StandardError{
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
