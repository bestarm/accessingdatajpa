package vn.vnpt.common.errorcode;

public class Code {
	public static final String SUCCESS = "IDG-0000000";
	public static final String CONTINUE = "IDG-00000100";
	public static final String SWITCHING_PROTOCOLS = "IDG-00000101";
	public static final String OK = "IDG-00000200";
	public static final String CREATED = "IDG-00000201";
	public static final String ACCEPT = "IDG-00000202";
	public static final String NON_AUTHORITATIVE_INFORMATION = "IDG-00000203";
	public static final String NO_CONTENT = "IDG-00000204";
	public static final String RESET_CONTENT = "IDG-00000205";
	public static final String PARTIAL_CONTENT = "IDG-00000206";
	public static final String MUTLTIPLE_CHOICES = "IDG-00000300";
	public static final String MOVED_PERMANENTLY = "IDG-00000301";
	public static final String FOUND = "IDG-00000302";
	public static final String SEE_OTHER = "IDG-00000303";
	public static final String NOT_MODIFIER = "IDG-00000304";
	public static final String USE_PROXY = "IDG-00000305";
	public static final String TEMPORARY_REDIRECT = "IDG-00000306";
	public static final String BAD_REQUEST = "IDG-00000400";
	public static final String UNAUTHORIZED = "IDG-00000401";
	public static final String PAYMENT_REQUIRED = "IDG-00000402";
	public static final String FOBIDDEN = "IDG-00000403";
	public static final String NOT_FOUND = "IDG-00000404";
	public static final String INTERNAL_SERVER_ERROR = "IDG-00000500";
	
}
