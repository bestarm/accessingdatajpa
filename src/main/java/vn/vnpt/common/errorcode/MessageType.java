package vn.vnpt.common.errorcode;

public enum MessageType {
	SUCCESS, INFO, WARNING, ERROR
}
