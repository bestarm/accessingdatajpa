package vn.vnpt.dto.in;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PutDtoInt {
	
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String uuidAccount;
	
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String firstName;
	
	@NotNull(message = "IDG-00000001")
	@Size(min = 1, max = 255, message = "IDG-00000004")
	private String lastName;
	
	@Pattern(regexp="(^$|[0-9]{10})", message = "IDG-00000008")
	private String phoneNUmber;
	
	private String userName;
	
	public String getUuidAccount() {
		return uuidAccount;
	}
	public void setUuidAccount(String uuidAccount) {
		this.uuidAccount = uuidAccount;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNUmber() {
		return phoneNUmber;
	}
	public void setPhoneNUmber(String phoneNUmber) {
		this.phoneNUmber = phoneNUmber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
