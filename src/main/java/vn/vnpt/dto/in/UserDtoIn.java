package vn.vnpt.dto.in;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoIn {
	
	@NotNull
	@NotBlank(message = "{user.lastName.notBlank")
	@Size(min = 1, max = 10, message = "{user.lastName.size}")
    private String lastName;
	@NotNull
    private String middleName;
	@NotNull
    private String firstName;
	@NotNull
    private Integer siblings;
}
