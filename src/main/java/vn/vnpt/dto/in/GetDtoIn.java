package vn.vnpt.dto.in;

public class GetDtoIn {
	private String uuidAccount;

	public String getUuidAccount() {
		return uuidAccount;
	}

	public void setUuidAccount(String uuidAccount) {
		this.uuidAccount = uuidAccount;
	}
}
