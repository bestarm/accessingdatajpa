package vn.vnpt.dto.out;

public class GetDtoOut {
	private String uuidAccount;
	private String fullName;
	private String birthDay;
	private String gender;
	private String phoneNumber;
	
	public String getUuidAccount() {
		return uuidAccount;
	}
	public void setUuidAccount(String uuidAccount) {
		this.uuidAccount = uuidAccount;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
