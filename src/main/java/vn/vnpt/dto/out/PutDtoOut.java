package vn.vnpt.dto.out;

public class PutDtoOut {
	private String uuidAccount;
	private String fullName;
	public String getUuidAccount() {
		return uuidAccount;
	}
	public void setUuidAccount(String uuidAccount) {
		this.uuidAccount = uuidAccount;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	
}
